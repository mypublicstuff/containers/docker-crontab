FROM alpine:latest
MAINTAINER johan.bergquist@netip.se

ENV APP_USER crontab

# Create crontabuser
RUN adduser -g "Crontab User" -D $APP_USER
# Copy your crontab-file to crontab
COPY my-crontab /var/spool/cron/crontabs/$APP_USER
# Give execution rights on the cron job
RUN chmod 0600 /var/spool/cron/crontabs/$APP_USER
# Entrypoint config
ENTRYPOINT "crond"
CMD ["-f", "-d", "8"]
